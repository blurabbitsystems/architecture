import boto3
import time
import json

from ansible.module_utils.basic import AnsibleModule


class ElasticSearchDomain(object):
    _client = None
    _name = None
    _region = None
    _subnets = []
    _security_groups = []
    _domain = None

    def __init__(self, domain_name, subnet_id, security_group, aws_region):
        self._client = boto3.client('es')
        self._name = domain_name
        self._region = aws_region
        self._subnets = [subnet_id]
        self._security_groups = [security_group]

    def start(self):
        changed = True

        if self.state == 'absent':
            self._create_domain()
        elif self._requires_update():
            self._update_domain()
        else:
            changed = False

        while self.state == 'processing':
            time.sleep(30)

        return changed

    def terminate(self):
        changed = True

        if self.state != 'absent':
            self._delete_domain()
        else:
            changed = False

        while self.state == 'processing':
            time.sleep(30)

        return changed

    @property
    def endpoint(self):
        try:
            return self._domain['Endpoints']['vpc']
        except Exception:
            pass

    @property
    def state(self):
        self._lookup_domain()

        if not self._domain:
            return 'absent'

        if self._domain['Processing']:
            return 'processing'

        if self.endpoint:
            return 'present'

        return 'absent'

    def _requires_update(self):
        if self.state == 'unavailable':
            raise Exception('domain is not created yet')

        if self._domain['VPCOptions']['SubnetIds'] != self._subnets:
            return True
        
        if self._domain['VPCOptions']['SecurityGroupIds'] != self._security_groups:
            return True

        return False

    def _lookup_domain(self):
        try:
            domain = self._client.describe_elasticsearch_domain(DomainName=self._name)
            self._domain = domain['DomainStatus']
        except self._client.exceptions.ResourceNotFoundException:
            pass

    def _create_domain(self):
        self._client.create_elasticsearch_domain(**self._get_config())
        while self.state != 'present':
            time.sleep(30)

    def _update_domain(self):
        config = self._get_config()
        config.pop('ElasticsearchVersion')

        self._client.update_elasticsearch_domain_config(**config)
        while self.state != 'present':
            time.sleep(30)

    def _delete_domain(self):
        self._client.delete_elasticsearch_domain(DomainName=self._name)

    def _get_config(self):
        aws_account_id = boto3.client('sts').get_caller_identity()['Account']
        access_policy = {
            'Version': '2012-10-17',
            'Statement': [
                {
                    'Effect': 'Allow',
                    'Principal': {
                        'AWS': ['*']
                    },
                    'Action': ['es:*'],
                    'Resource': f'arn:aws:es:{self._region}:{aws_account_id}:domain/{self._name}/*'
                }
            ]
        }

        return dict(
            DomainName=self._name,
            ElasticsearchVersion='7.4',
            ElasticsearchClusterConfig={
                'InstanceType': 't2.small.elasticsearch',
                'InstanceCount': 1,
                'DedicatedMasterEnabled': False,
                'ZoneAwarenessEnabled': False,
            },
            EBSOptions={
                'EBSEnabled': True,
                'VolumeType': 'gp2',
                'VolumeSize': 10,
            },
            VPCOptions={
                'SubnetIds': self._subnets,
                'SecurityGroupIds': self._security_groups,
            },
            DomainEndpointOptions={
                'EnforceHTTPS': False,
            },
            AccessPolicies=json.dumps(access_policy),
        )

    def __repr__(self):
        return f'aws-es-domain[name={self._name}, state={self.state}]'


if __name__ == '__main__':
    module_args = dict(
        name=dict(type='str', required=True),
        subnet_id=dict(type='str', required=True),
        security_group=dict(type='str', required=True),
        aws_region=dict(type='str', required=True),
        state=dict(type='str', required=False, default='present')
    )

    module = AnsibleModule(argument_spec=module_args)
    es_domain = ElasticSearchDomain(
        domain_name=module.params['name'],
        subnet_id=module.params['subnet_id'],
        security_group=module.params['security_group'],
        aws_region=module.params['aws_region'],
    )

    changed = False

    if module.params['state'] in ['present', 'started']:
        changed = es_domain.start()
    
    elif module.params['state'] in ['absent', 'stopped']:
        changed = es_domain.terminate()

    else:
        module.fail_json(msg='invalid state value, valid values: present|started|absent|stopped')
    
    result = dict(
        changed=changed,
        endpoint=es_domain.endpoint,
    )
    
    module.exit_json(**result)
