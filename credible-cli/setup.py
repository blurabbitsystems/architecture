import setuptools

setuptools.setup(
    name='credible',
    version='0.0.1',
    author='Ahmed Sayeed Wasif',
    author_email='aswasif007@gmail.com',
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    scripts=[
        'bin/credible-git',
    ],
)
