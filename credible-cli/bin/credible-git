#!/bin/python

import click
import json
import os

from git import Repo, GitCommandError


WORKSPACE = None
GIT_PROJECT_ROOT = 'git@bitbucket.org:blurabbitsystems'
REPOSITORIES = [
    'public-content-server',
    'application-interface',
    'user-manager',
]


@click.group()
def cli():
    _load_config()


@cli.command()
def clonerepo():
    for repo in REPOSITORIES:
        _clone_repo(repo)


def _clone_repo(repo_name):
    repo_dir = f'{WORKSPACE}/{repo_name}'
    if os.path.isdir(repo_dir) and os.listdir(repo_dir):
        click.echo(f'Skipping git-clone as directory is not empty: {repo_dir}')
        return

    try:
        click.echo(f'Cloning {repo_name} at {repo_dir}')
        Repo.clone_from(f'{GIT_PROJECT_ROOT}/{repo_name}.git', repo_dir)
    except GitCommandError as e:
        click.echo(e)


def _load_config():
    global WORKSPACE

    config = None

    try:
        with open(os.path.expanduser('~/.credible.json')) as fs:
            config = json.load(fs)
    except FileNotFoundError:
        click.echo('Configuration file ~/.credible.json does not exist.')
        exit(1)
    except json.decoder.JSONDecodeError:
        click.echo('Configuration file ~/.credible.json does not contain valid json.')
        exit(1)

    if 'workspace' not in config:
        click.echo('~/.credible.json does not have key \'workspace\'')
        exit(1)

    WORKSPACE = os.path.expanduser(config['workspace'])

    return config


if __name__ == '__main__':
    cli()
